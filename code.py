import sys, math, operator, copy

# It's the survival of the biggest!
# Propel your chips across a frictionless table top to avoid getting eaten by bigger foes.
# Aim for smaller oil droplets for an easy size boost.
# Tip: merging your chips will give you a sizeable advantage.


####################################################################################################
####################################         MAIN       ############################################

def main():
    # initialisation des joueurs
    joueurs = {-1:Joueur(-1,False,{}), 0:Joueur(0,True,{}), 1:Joueur(1,True,{}), 2:Joueur(2,True,{}), 3:Joueur(3,True,{}), 4:Joueur(4,True,{})}
    playerId = int(input()) # your id (0 to 4)
    jetonsActifs = {} # jetons encore vivants
    jetonsProchesTemps = {} # jetons proche de mon jeton en cours
    # game loop
    while 1:
        playerChipCount = int(input()) # The number of chips under your control
        entityCount = int(input()) # The total number of entities on the table, including your chips
        jetonsActifs.clear()
        #perdreTemps(75) # (75) fonction servant juste � perdre du temps pour avoir une estimation du temps restant
        for i in range(entityCount):
            (id, player, radius, x, y, vx, vy) = recupererJetons()
            joueurs[player].majJeton(id,radius,x,y,vx,vy)
            jetonsActifs[id] = joueurs[player].jetons[id]
        # nombre de joueurs en vie restants
        Joueur.epurer(joueurs,set(jetonsActifs))
        print("Il reste {} joueurs en vie : {}".format(len(joueurs)-1, set(joueurs)-{-1}), file=sys.stderr)
        # ordres donn�s aux jetons
        for monJetonId, monJeton in joueurs[playerId].jetons.items():
            autresJetons = Jeton.autresJetons(monJetonId,jetonsActifs)
            jetonsProchesTemps.clear()
            for id, jeton in autresJetons.items():
                (possible,temps) = monJeton.peutIntersecter(jeton)
                if possible:
                    jetonsProchesTemps[id] = temps
            print("Mon jeton {} peut intercepter : {}".format(monJetonId,jetonsProchesTemps), file=sys.stderr)
            # Si il y a des jetons proches, prendre une d�cision
            if len(jetonsProchesTemps)>0:
                monJeton.decisionProche(jetonsProchesTemps,jetonsActifs)
            
        # tri par ordre croissant de mes jetons
        mesJetonsIdTries = sorted(joueurs[playerId].jetons.keys())
        for jetonId in mesJetonsIdTries:
            monJeton = jetonsActifs[jetonId]
            if monJeton.enAttente :
                print("WAIT")
                print("WAIT", file=sys.stderr)
            else:
                print("{} {}".format(monJeton.direction.m[0][0], monJeton.direction.m[1][0]))
                print("{} {}".format(monJeton.direction.m[0][0], monJeton.direction.m[1][0]), file=sys.stderr)



####################################################################################################
####################################         Joueur       ##########################################

class Joueur:
    """
    Classe d�crivant un joueur de PokerChipRace.
    Attributs :
        id : int donnant l'id du joueur (entre -1 et 4)
        estIA : Boolean indique si c'est une IA ou un joueur passif
        aGagne : Boolean indique si le joueur a gagn� d'avance cette partie
        jetons : dictionnaire des jetons en sa possession
    """
    
    def __init__(self, id, estIA, jetons):
        """ Constructeur d'un Joueur """
        self.id = id
        self.estIA = estIA
        self.aGagne = False
        self.jetons = jetons
    
    def nbJetons(self):
        """ Renvoie le nombre de jetons que poss�de le joueur """
        return len(self.jetons)
    
    def garderJetons(self,jetonsId):
        """ Ne garde que les jetons dont l'id est dans le set jetonsId """
        self.jetons = {cle:self.jetons[cle] for cle in set(self.jetons)&jetonsId}
    
    def majJeton(self,id,rayon,x,y,vx,vy):
        """ Met � jour le jeton id du joueur (ou le cr�e s'il n'existe pas) """
        if id in self.jetons:
            self.jetons[id].maj(rayon,x,y,vx,vy)
        else:
            position = Vecteur(x=x,y=y)
            vitesse = Vecteur(x=vx,y=vy)
            self.jetons[id] = Jeton(id,self,rayon,position,vitesse)
    
    @staticmethod
    def epurer(joueurs, jetonsActifsId):
        joueursId = set(joueurs) - {-1}
        for id in joueursId:
            if len(joueurs[id].jetons)==0:
                joueurs.pop(id,None)
            else:
                joueurs[id].garderJetons(jetonsActifsId)
                #print("{} : poss�de {} jetons".format(id,joueurs[id].nbJetons()), file=sys.stderr)

####################################################################################################
####################################         Jeton       ###########################################

class Jeton:
    """
    Classe d�crivant un jeton.
    Attributs :
        id : int donnant l'id du jeton
        estPassif : Boolean indique si le jeton est passif ou dirig� par une IA
        possesseur : Joueur le joueur poss�dant le jeton
        rayon : float le rayon du jeton
        position : Vecteur les coordonn�es du jeton
        vitesse : Vecteur la vitesse de d�placement du jeton
        direction : Vecteur la position vis�e
        enAttente : boolean indique si le jeton attend ou donne un ordre de propulsion
    """
    
    def __init__(self, id, possesseur, rayon, position, vitesse):
        """ Constructeur d'un Jeton """
        self.id = id
        self.possesseur = possesseur
        self.estPassif = (possesseur.id == -1)
        self.rayon = rayon
        self.position = position
        self.vitesse = vitesse
        self.direction = Vecteur(x=0,y=0)
        self.enAttente = True
    
    def maj(self,rayon,x,y,vx,vy):
        """ Met � jour le jeton (le remet aussi en attente pour qu'il ne se repropulse pas) """
        position = Vecteur(x=x,y=y)
        vitesse = Vecteur(x=vx,y=vy)
        self.rayon = rayon
        self.position = position
        self.vitesse = vitesse
        self.enAttente = True
    
    @staticmethod
    def autresJetons(jetonId,jetonsActifs):
        return {id:jetonsActifs[id] for id in set(jetonsActifs) if id != jetonId}

    def peutIntersecter(self, jeton):
        """ Indique s'il est possible (mais pas s'il est s�r) d'intercepter ce jeton avant le prochain temps.
        Si c'est possible, pr�cise le temps minimal pour le faire. """
        # variables qui seront renvoy�es
        possible = False
        t = -1
        # premi�re v�rif n�c�ssaire simple avec les distances
        if Jeton.possibleZoneCommune(self, jeton):
            (a,b,c) = Jeton.polynomeIntersection(self,jeton)
            # on regarde d'abord les cas d�g�n�r�s
            if a==0:
                # si la pente est croissante, comme P(0)>0 il ne peut pas y avoir d'intersection : on ne regarde que les pentes d�croissantes
                if b<0:
                    #print("cas a=0", file=sys.stderr)
                    t = -c/b
                    possible = (0<t<1)
            # cas non-d�g�n�r�
            else:
                racines = racinesPolynome(a,b,c)
                # P(0)>0 donc on ne s'int�resse pas � tous les cas
                if len(racines)==2:
                    if a>0:
                        #print("cas a>0", file=sys.stderr)
                        #print("a = {} ; b = {} ; c = {}".format(a,b,c), file=sys.stderr)
                        t = racines[0]
                        possible = (0<t<1)
                    else:
                        #print("cas a<0", file=sys.stderr)
                        t = racines[1]
                        possible = (0<t<1)
        return (possible,t)

    def distanceAtteignable(self):
        """ Indique la distance max d'un objet atteignable """
        r1 = self.rayon
        r2 = self.estPassif and r1 or math.sqrt(14/15)*r1 + 200/14
        return self.vitesse.norme() + r2
    
    @staticmethod
    def possibleZoneCommune(jeton1, jeton2):
        """ Condition n�cessaire pour que 2 jetons puissent s'intersecter """
        d1 = jeton1.distanceAtteignable()
        d2 = jeton2.distanceAtteignable()
        d = (jeton1.position - jeton2.position).norme()
        return d < d1+d2
    
    @staticmethod
    def polynomeIntersection(jeton1, jeton2):
        """ renvoie a, b et c sachant qu'il y a intersection si P(t)<0 o� P(t) = a*t2 + b*t + c """
        # variables des jetons
        X1 = jeton1.position
        X2 = X1 + jeton1.vitesse
        r1 = jeton1.rayon
        r2 = jeton1.estPassif and r1 or math.sqrt(14/15)*r1 + 200/14
        X1p = jeton2.position
        X2p = X1p + jeton2.vitesse
        r1p = jeton2.rayon
        r2p = jeton2.estPassif and r1p or math.sqrt(14/15)*r1p + 200/14
        # variables facilitant le calcul des coeffs
        D1 = X1-X1p
        D2 = X2-X2p
        R1 = r1+r1p
        R2 = r2+r2p
        # calul des coeffs
        a = (D2-D1).norme2() - (R2-R1)*(R2-R1)
        b = 2*(D1.dot(D2-D1) - R1*(R2-R1))
        c = D1.norme2() - R1*R1
        return (a,b,c)
    
    def decisionProche(self, jetonsProchesTemps, jetonsActifs):
        """ donne l'ordre en fonction de la situation avec les jetons proches (temporellement) """
        if len(jetonsProchesTemps)>0:
            jetonsProches = {id:jetonsActifs[id] for id in set(jetonsProchesTemps)}
            # s�parer les jetons en 2 cat�gories : les plus gros et les plus petits
            jetonsGros = {id:jetonsProches[id] for id in set(jetonsProchesTemps) if (jetonsProches[id].rayon > self.rayon) & (jetonsProches[id].possesseur.id != self.possesseur.id)}
            jetonsPetits = {id:jetonsProches[id] for id in set(jetonsProchesTemps) if jetonsProches[id].rayon < self.rayon}
            # Si je risque de rencontrer un plus gros jeton, priorit� � l'�vitement
            if len(jetonsGros)>0:
                self.fuir(jetonsGros,jetonsProchesTemps)
            # Sinon je choisis la meilleure direction (ou pas de propulsion)
            else:
                self.meilleureDirection(jetonsPetits, jetonsProchesTemps)
    
    def fuir(self, jetonsGros, jetonsProchesTemps):
        """ Change la consigne pour fuir au mieux � ces gros jetons """
        if len(jetonsGros)>0:
            dangerosites = {}
            for id, jeton in jetonsGros.items():
                dangerosites[id] = self.dangerosite(jeton, jetonsProchesTemps[id])
            self.estimerObjectif(dangerosites,jetonsGros)
    
    def meilleureDirection(self, jetonsPetits, jetonsProchesTemps):
        """ Choisit la meilleur direction � prendre entre :
            - meilleur gain passif
            - meilleur gain actif (avec propulsion) """
        if len(jetonsPetits)>0:
            # gain indiquant s'il vaut la peine de se d�placer activement (avec propulsion)
            gain = 0
            gainPassif = 0
            gainActif = 0
            perteActive = (self.rayon * self.rayon)/15
            idActif = 0
            # On regarde le meilleur jeton atteignable sans propulsion et avec propulsion
            jetonsAtteignablesPassivementId = set()
            jetonsAtteignablesActivementId = set()
            monJetonCopie = copy.deepcopy(self)
            jetonsCopies = copy.deepcopy(jetonsPetits)
            for jetonId,jeton in jetonsCopies.items():
                monJetonCopie.estPassif = True
                jeton.estPassif = True
                (possible,temps) = monJetonCopie.peutIntersecter(jeton)
                if possible:
                    jetonsAtteignablesPassivementId.add(jetonId)
                else:
                    monJetonCopie.estPassif = False
                    (possible,temps) = monJetonCopie.peutIntersecter(jeton)
                    if possible:
                        jetonsAtteignablesActivementId.add(jetonId)
            # calcul des gains dans chaque cat�gorie
            for id in jetonsAtteignablesPassivementId:
                gainTemp = jetonsPetits[id].rayon * jetonsPetits[id].rayon
                gain = max(gainTemp, gain)
                gainPassif = gain
            for id in jetonsAtteignablesActivementId:
                if jetonsPetits[id].rayon * jetonsPetits[id].rayon < 14*(self.rayon * self.rayon)/15:
                    gainActifTemp = jetonsPetits[id].rayon * jetonsPetits[id].rayon - perteActive
                    gainActif = max(gainActifTemp,gainActif)
                    if gainActif > gain:
                        idActif = id
                        gain = gainActif
            # si on a un meilleur gain actif, on cible le jeton permettant ce gain
            if gain > gainPassif:
                self.cibler(jetonsPetits[idActif])
	
    def cibler(self, jeton):
        """ Choisit la meilleur trajectoire pour manger le jeton
        Il va falloir tenir compte de si c'est un joueur actif ou passif :
        s'il est passif on va essayer de modifier au minimal notre trajectoire (en fait on s'en fou dans un premier temps) """
        # retour dans le rep�re original
        cible = self.position + self.vitesseVersCible(jeton)
        self.direction = cible
        self.enAttente = False
    
    def vitesseVersCible(self, jeton):
        # Passage dans le rep�re de mon jeton
        X1p = jeton.position - self.position
        Vp = jeton.vitesse - self.vitesse
        """ Si on note C(t) la position du centre du jeton � l'instant t, on a C(t)=X1p+t*Vp
        On suppose que l'on vise le point C(t). Je note d la longueur de notre d�placement (d=200/14)
        Alors, � l'instant t, on s'est d�plac� de la distance d vers le point C(t).
        Le probl�me est donc de minimiser :
        abs( ||C(t)|| - d*t )
        On va s'y prendre en �valuant 10 t diff�rents et on choisira le meilleur """
        temps = [t/10 for t in range(11)]
        meilleurTemps = 0
        delta = X1p.norme()
        d = 200/14
        for t in temps:
            C = X1p + t*Vp
            deltaTemp = abs(C.norme() - t*d)
            if deltaTemp<delta :
                meilleurTemps = t
                delta = deltaTemp
        cible = X1p + t*Vp
        return (1/cible.norme())*cible
    
    def dangerosite(self,jeton,temps):
        """ Indique la dangerosit� d'un jeton, plus il est gros et proche, plus il est dangereux """
        # rapport des masses
        rm = (jeton.rayon * jeton.rayon)/(self.rayon * self.rayon)
        return rm/temps
    
    def estimerObjectif(self,coeffs,jetons):
        """ A FAIRE : d�pend des cas """
        objectif = Vecteur()
        sommeCoeffs = 0
        for id, jeton in jetons.items():
            objectif = objectif + (coeffs[id] * self.posFuite(jeton))
            sommeCoeffs += coeffs[id]
        objectif = (1/sommeCoeffs) * objectif
        self.direction = objectif
        self.enAttente = False
    
    def vitesseFuite(self, jeton):
        """ d�termine la vitesse � donner pour fuir le jeton """
        # Passage dans le rep�re de mon jeton
        X1p = jeton.position - self.position
        Vp = jeton.vitesse - self.vitesse
        # D�placement normal
        N = Vecteur(x=Vp.m[1][0],y=-Vp.m[0][0])
        norme = N.norme()
        if norme != 0:
            N = (1/norme) * N
            if (N.dot(X1p) > 0):
                N = (-1)*N
        D = (-1/X1p.norme())*X1p
        fuite = D+N
        fuite = (1/fuite.norme()) * fuite
        return fuite
    
    def posFuite(self, jeton):
        """ d�termine la position optimale � viser pour fuir le jeton """
        return self.position - self.vitesseVersCible(jeton)
    
####################################################################################################
#################################        Math�matiques       #######################################

def racinesPolynome(a,b,c):
    """ Renvoie un tuple contenant les racines d'un polyn�me de la forme P(x) = a*x2 + b*x + c """
    d = b*b - 4*a*c
    if d<0:
        return ()
    elif d==0:
        return (-b/(2*a) ,)
    else:
        r1 = (-b-math.sqrt(d))/(2*a)
        r2 = (-b+math.sqrt(d))/(2*a)
        if r1>r2:
            r1,r2 = r2,r1
        return (r1,r2)

class Matrice:
    """
    Classe d�crivant une matrice.
    Attributs :
        lignes : int le nombre de lignes
        colonnes : int le nombre de colonnes
        m : la liste 2D (matrice) de float
    """
    
    def __init__(self,lignes,colonnes):
        """ Constructeur d'une matrice """
        self.lignes = lignes
        self.colonnes = colonnes
        self.m = [[0] * colonnes for _ in range(lignes)]

    def __add__(self,m2):
        """ Addition de 2 matrices """
        if (self.lignes != m2.lignes) | (self.colonnes != m2.colonnes) :
            raise Exception("Probl�me de dimension dans l'addition des matrices. Les tailles des matrices sont respectivement : {} et {}".format((self.lignes,self.colonnes),(m2.lignes,m2.colonnes)))
        m3 = Matrice(self.lignes,self.colonnes)
        for i in range(m3.lignes):
            for j in range(m3.colonnes):
                m3.m[i][j] = self.m[i][j] + m2.m[i][j]
        return m3
    
    def __radd__(self,s):
        """ Addition d'un scalaire avec la matrice : s+M """
        mat = Matrice(self.lignes,self.colonnes)
        for i in range(mat.lignes):
            for j in range(mat.colonnes):
                mat.m[i][j] = s + self.m[i][j]
        return mat
        
    def __sub__(self,m2):
        """ Soustraction d'une seconde matrice """
        if (self.lignes != m2.lignes) | (self.colonnes != m2.colonnes) :
            raise Exception("Probl�me de dimension dans la soustraction des matrices. Les tailles des matrices sont respectivement : {} et {}".format((self.lignes,self.colonnes),(m2.lignes,m2.colonnes)))
        m3 = Matrice(self.lignes,self.colonnes)
        for i in range(m3.lignes):
            for j in range(m3.colonnes):
                m3.m[i][j] = self.m[i][j] - m2.m[i][j]
        return m3
        
    def __mul__(self,m2):
        """ Multiplication de 2 matrices """
        if (self.colonnes != m2.lignes):
            raise Exception("Probl�me de dimension dans la multiplication des matrices. Les tailles des matrices sont respectivement : {} et {}".format((self.lignes,self.colonnes),(m2.lignes,m2.colonnes)))
        m3 = Matrice(self.lignes,m2.colonnes)
        for i in range(m3.lignes):
            for j in range(m3.colonnes):
                e = 0
                for k in range(self.colonnes):
                    e += self.m[i][k] * m2.m[k][j]
                m3.m[i][j] = e
        return m3

    def __rmul__(self,s):
        """ r�sultat de s*M ou s est un scalaire """
        mat = Matrice(self.lignes,self.colonnes)
        for i in range(mat.lignes):
            for j in range(mat.colonnes):
                mat.m[i][j] = s * self.m[i][j]
        return mat
    
    def dot(self,m2):
        """ produit scalaire de m et m2 suivant la premi�re dimension """
        mat = Matrice(1,self.colonnes)
        for j in range(mat.colonnes):
            s = 0
            for i in range(self.lignes):
                s += self.m[i][j] * m2.m[i][j]
            mat.m[0][j] = s
        return mat
    
    def norme2(self):
        """ Renvoie la norme au carr� d'une matrice (somme des carr�s des coeffs) """
        mCarre = [[x*x for x in l] for l in self.m]
        norme = sum([sum(lCarre) for lCarre in mCarre])
        """# v�rification des erreurs
        somme = 0;
        for i in range(self.lignes):
            for j in range(self.colonnes):
                somme += self.m[i][j] * self.m[i][j]
        if norme != somme :
            print("Norme2 mauvaise : {} != {}".format(norme,somme), file=sys.stderr)"""
        return norme
        
    def norme(self):
        """ Renvoie la norme d'une matrice """
        return math.sqrt(self.norme2())
        
class Vecteur(Matrice):
    """
    Classe d�finissant un vecteur (qui h�rite de matrice)
    """
    
    def __init__(self,lignes=2,colonnes=1,x=0,y=0):
        """ Constructeur d'un Vecteur """
        Matrice.__init__(self,lignes,colonnes)
        self.m[0][0] = x
        self.m[1][0] = y
    
    def __add__(self,m2):
        """ Addition de 2 matrices """
        temp = Matrice.__add__(self,m2)
        temp.__class__ = self.__class__
        return temp
    
    def __radd__(self,s):
        """ Addition d'un scalaire avec la matrice : s+M """
        temp = Matrice.__radd__(self,s)
        temp.__class__ = self.__class__
        return temp
        
    def __sub__(self,m2):
        """ Soustraction d'une seconde matrice """
        temp = Matrice.__sub__(self,m2)
        temp.__class__ = self.__class__
        return temp
        
    def __mul__(self,m2):
        """ Multiplication de 2 matrices """
        temp = Matrice.__mul__(self,m2)
        temp.__class__ = self.__class__
        return temp

    def __rmul__(self,s):
        """ r�sultat de s*M ou s est un scalaire """
        temp = Matrice.__rmul__(self,s)
        temp.__class__ = self.__class__
        return temp
        
    def dot(self,v):
        """ Produit scalaire entre 2 vecteurs """
        s = 0
        for i in range(self.lignes):
            s += self.m[i][0] * v.m[i][0]
        return s

####################################################################################################
###################################         Codingame       ########################################

def recupererJetons():
    """ renvoie les diff�rents param�tres d'un jeton """
    # id: Unique identifier for this entity
    # player: The owner of this entity (-1 for neutral droplets)
    # radius: the radius of this entity
    # x: the X coordinate (0 to 799)
    # y: the Y coordinate (0 to 514)
    # vx: the speed of this entity along the X axis
    # vy: the speed of this entity along the Y axis
    id, player, radius, x, y, vx, vy = input().split()
    id = int(id)
    player = int(player)
    radius = float(radius)
    x = float(x)
    y = float(y)
    vx = float(vx)
    vy = float(vy)
    return (id, player, radius, x, y, vx, vy)

def perdreTemps(n):
    """ fonction ayant pour unique but de perdre du temps """
    m1 = Matrice(n,n)
    m2 = Matrice(n,n)
    m3 = m1*m2
    del m1
    del m2
    del m3

# lancement du main � la fin
if __name__ == '__main__':
    main()