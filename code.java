import java.util.*;
import java.io.*;
import java.math.*;

/**
 * It's the survival of the biggest!
 * Propel your chips across a frictionless table top to avoid getting eaten by bigger foes.
 * Aim for smaller oil droplets for an easy size boost.
 * Tip: merging your chips will give you a sizeable advantage.
 **/
class Player {
    
    public int id;
    public Map<Integer,Jeton> jetons;
    
    public Player(int id){
        this.id = id;
        this.jetons = new HashMap();
    }
    
    public static void majPlayer(Player[] joueurs, int id, int player, double radius, double x, double y, double vx, double vy) {
        Player p;
        if (player == -1) {
            player = 5;
        }
        //System.err.println("player : " + player + "; id : " + id);
        p = joueurs[player];
        
        // si le jeton existe d�j�, on le met juste � jour
        if (p.jetons.containsKey(id)) {
            p.jetons.get(id).majJeton(radius,x,y,vx,vy);
        // sinon on le cr�e
        } else {
            p.jetons.put(id,new Jeton(id, player, radius, x, y, vx, vy));
        }
        
    }
    
    // affiche l'�tat d'un joueur
    public void printState(Set<Integer> jetonsActifs) {
        Set<Integer> jetonsSet = this.jetons.keySet();
        jetonsSet.retainAll(jetonsActifs);
        if (jetonsSet.size()>0) {
            System.err.println("player " + this.id + " : " + jetonsSet.size() + " jetons");
            System.err.println(jetonsSet);
            System.err.println("");
        }
    }
    
    // calcul le mouvement � imposer � mon jeton
    public static void estimerMouvementCourt(Jeton monJeton, Map<Integer,Jeton> jetonsSusceptibles, Map<Integer,Double> proximites) throws Exception {
        double scoreRayon;
        double scoreProximite;
        double proximite;
        double score;
        Map<Integer,Double> scores = new HashMap();
        // donner un score � chaque jeton � proximit�
        for (Map.Entry<Integer, Jeton> sonJeton : jetonsSusceptibles.entrySet()) {
            scoreRayon = estimerScoreRayon(monJeton.radius, sonJeton.getValue().radius);
            proximite = proximites.get(sonJeton.getKey());
            scoreProximite = 1-proximite;
            score = scoreRayon*scoreProximite;
            if (sonJeton.getValue().player == monJeton.player) {
                score = -score;
            } else if (sonJeton.getValue().player != 5) {
                score = 1.2*score;
            }
            scores.put(sonJeton.getKey(),Double.valueOf(score));
        }
        // trier les scores
        TreeMap<Integer,Double> scoresTries = new TreeMap<Integer,Double>(scores);
        Map.Entry<Integer,Double> meilleurScore = scoresTries.lastEntry();
        Map.Entry<Integer,Double> pireScore = scoresTries.firstEntry();
        monJeton.seDeplacer = false;
        // action li�e au pire score
        if (pireScore != null && pireScore.getValue() < 0 ) {
            Jeton pireJeton = jetonsSusceptibles.get(pireScore.getKey());
            // si le pire score est n�gatif il faut fuire
            if ( pireJeton.player != monJeton.player) {
                monJeton.seDeplacer = true;
                monJeton.posVisee = estimerFuite(monJeton,pireJeton);
            }
        // action li�e au meilleur score
        } else if (meilleurScore != null) {
            Jeton meilleurJeton = jetonsSusceptibles.get(meilleurScore.getKey());
            // si le meilleur score est un de mes jetons (plus gros) alors je vais le rencontrer
            if (meilleurJeton.player == monJeton.player && meilleurJeton.radius > monJeton.radius) {
                monJeton.seDeplacer = true;
                monJeton.posVisee = estimerIntersection(monJeton,meilleurJeton,1.0);
                System.err.println("Jeton " + monJeton.id + " :" + monJeton.position + " " + meilleurJeton.position + " " + monJeton.posVisee);
            // si score positif, je vais aussi le manger
            } else if (meilleurScore.getValue()>0.03) {
                monJeton.seDeplacer = true;
                monJeton.posVisee = estimerIntersection(monJeton,meilleurJeton,1.0);
                System.err.println("Jeton " + monJeton.id + " :" + monJeton.position + " " + meilleurJeton.position + " " + monJeton.posVisee);
            }
        }
        System.err.println(scores);
    }
    
    public static double estimerScoreRayon(double monRayon, double sonRayon){
        double score = 0;
        if (sonRayon<0.933*monRayon) {
            score = 15/(14*monRayon);
        } else {
            score = 15*(monRayon-sonRayon)/monRayon;
        }
        return score;
    }
    
    // estimation d'une position int�ressante pour intersecter l'autre jeton
    public static Matrix estimerIntersection(Jeton monJeton, Jeton autreJeton, Double etapes) throws Exception {
        Matrix p1 = monJeton.position;
        Matrix v1 = monJeton.vitesse;
        Matrix p2 = autreJeton.position;
        Matrix v2 = autreJeton.vitesse;
        Matrix delta = p1.plus(v1.times(etapes)).moins(p2).moins(v2.times(etapes));
        return p1.moins(delta);
    }
    
    // estimation d'une position de fuite
    public static Matrix estimerFuite(Jeton monJeton, Jeton autreJeton) throws Exception {
        Matrix p1 = monJeton.position;
        Matrix v1 = monJeton.vitesse;
        Matrix p2 = autreJeton.position;
        Matrix v2 = autreJeton.vitesse;
        Matrix delta = p1.plus(v1).moins(p2).moins(v2);
        return p1.plus(delta);
    }
    
    // estimation d'un d�placement en prenant en compte les objets �loign�s
    public static void estimerMouvementLong(Jeton monJeton, Map<Integer,Jeton> jetonsActifs) throws Exception {
        /**
         * Je consid�re l'ensemble des �l�ments plus petits que moi :
         * S'il est vide, je ne fais rien
         * Sinon Si on note Xi, Vi, ri, les positions, vitesses, et rayons de ces jetons (resp. prime pour le tour prochain)
         * PIi (resp. prime pour le tour suivant) un poids attribu� en fonction de la distance.
         * Soit C = 1/Somme(PIi) * Somme(PIi*score(ri) Xi)       (resp. Cprime)
         * estimer s'il vaut la peine de se d�placer vers Cprime
         */
        // Jetons plus petits
        Map<Integer,Jeton> jetonsPetits = calculerPetitsJetons(monJeton, jetonsActifs);
        // variables au temps pr�sent
        Map<Integer,Double> poids0 = calculerPoids(monJeton, jetonsPetits, 1.0);
        Map<Integer,Double> score0 = calculerScores(monJeton, jetonsPetits);
        Map<Integer,Matrix> pos0 = calculerPos(jetonsPetits, 1.0);
        Matrix C0 = calculerBarycentre(pos0,poids0,score0);
        double var0 = calculerVariance(C0,pos0,poids0,score0);
        System.err.println("Jeton " + monJeton.id + " : variance : " + var0);
        
        /*
        // variables au prochain temps
        Map<Integer,Double> poids1 = new HashMap<Integer,Double>();
        Map<Integer,Double> score1 = new HashMap<Integer,Double>();
        Matrix C1;
        double var1;
        */
        if (var0<60000) {
            /**
             * Si la variance est < � ce seuil, on cherche les points que je suis susceptible de rencontrer
             * en me dirigeant vers ce point.
             * Si le premier point que je risque de rencontrer est dangereux, je n'y vais pas !
             * mais s'il est b�n�fique, mangeaillons !
             */
            Map<Integer,Jeton> jetonsSusceptibles = new HashMap();
            SortedMap<Integer,Double> proximites = new TreeMap();
            String jetonsSusceptiblesString = "";
            double grandissement = Math.max(monJeton.position.moins(C0).norme()/monJeton.vitesse.norme(),1);
            
            jetonsSusceptiblesString = "( ";
            for (Map.Entry<Integer, Jeton> jeton : jetonsActifs.entrySet()) {
                if (!jeton.getKey().equals(monJeton.id)) {
                    double t = monJeton.rencontre(jeton.getValue(),grandissement);
                    if (t!=-1) {
                        jetonsSusceptiblesString += jeton.getKey() + " : " + String.format( "%.2f",t) + "; ";
                        jetonsSusceptibles.put(jeton.getKey(),jeton.getValue());
                        proximites.put(jeton.getKey(),Double.valueOf(t));
                    }
                }
            }
            jetonsSusceptiblesString += ")";
            System.err.println(jetonsSusceptiblesString);
            
            // on regarde si l'�l�ment le plus pr�s est dangereux
            if (proximites.size()>0) {
                double rayonPlusProche = jetonsSusceptibles.get(proximites.firstKey()).radius;
                if (rayonPlusProche < 0.933*monJeton.radius) {
                    monJeton.seDeplacer = true;
                    monJeton.posVisee = estimerIntersection(monJeton,jetonsSusceptibles.get(proximites.firstKey()),1.0);
                }
            }
        }
    }
    
    // trouver les jetons plus petits que le mien
    public static Map<Integer,Jeton> calculerPetitsJetons(Jeton monJeton, Map<Integer,Jeton> jetonsActifs) {
        Map<Integer,Jeton> jetonsPetits = new HashMap<Integer,Jeton>();
        for (Map.Entry<Integer,Jeton> jetonActif : jetonsActifs.entrySet()) {
            if (jetonActif.getValue().radius < monJeton.radius) {
                jetonsPetits.put(jetonActif.getKey(),jetonActif.getValue());
            }
        }
        return jetonsPetits;
    }
    
    // calculer des poids � attribuer aux jetons (d�pend de la distance � notre jeton)
    // etapes correspond � dans combien de tours on veut l'�valuer
    public static Map<Integer,Double> calculerPoids(Jeton monJeton, Map<Integer,Jeton> jetonsPetits, Double etapes) throws Exception {
        Map<Integer,Double> poids = new HashMap<Integer,Double>();
        for (Map.Entry<Integer,Jeton> jeton : jetonsPetits.entrySet()) {
            Double distance = Double.valueOf(monJeton.position.plus(monJeton.vitesse.times(etapes)).moins(jeton.getValue().position.plus(jeton.getValue().vitesse.times(etapes))).norme());
            Double poid = 1/distance;
            poids.put(jeton.getKey(),poid);
        }
        return poids;
    }
    
    // calculer des scores � attribuer aux jetons (en fonction de leur rayon)
    public static Map<Integer,Double> calculerScores(Jeton monJeton, Map<Integer,Jeton> jetonsPetits) throws Exception {
        Map<Integer,Double> scores = new HashMap<Integer,Double>();
        for (Map.Entry<Integer,Jeton> jeton : jetonsPetits.entrySet()) {
            Double score = Double.valueOf(estimerScoreRayon(monJeton.radius, jeton.getValue().radius));
            scores.put(jeton.getKey(),score);
        }
        return scores;
    }
    
    // Renvoyer les positions de jetons
    public static Map<Integer,Matrix> calculerPos(Map<Integer,Jeton> jetonsPetits, Double etapes) throws Exception {
        Map<Integer,Matrix> positions = new HashMap<Integer,Matrix>();
        for (Map.Entry<Integer,Jeton> jeton : jetonsPetits.entrySet()) {
            Matrix pos = jeton.getValue().position.plus(jeton.getValue().vitesse.times(etapes));
            positions.put(jeton.getKey(),pos);
        }
        return positions;
    }
    
    // calculer un barycentre pond�r�
    public static Matrix calculerBarycentre(Map<Integer,Matrix> pos, Map<Integer,Double> poids, Map<Integer,Double> score) throws Exception {
        Double sommePoids = 0.;
        Matrix bary = new Matrix(2,1);
        for (int id : pos.keySet()){
            sommePoids += poids.get(id) * score.get(id);
            bary = bary.plus(pos.get(id).times(poids.get(id) * score.get(id)));
        }
        return bary.times(1/sommePoids);
    }
    
    // calcul de l'�cart des jetons � la moyenne pond�r�e
    public static double calculerVariance(Matrix centre, Map<Integer,Matrix> positions, Map<Integer,Double> poids, Map<Integer,Double> score) throws Exception {
        double var = 0.;
        double norme2 = 0.;
        Double sommePoids = 0.;
        for (int id : positions.keySet()){
            sommePoids += poids.get(id) * score.get(id);
            norme2 = centre.moins(positions.get(id)).normeCarre();
            var += poids.get(id) * score.get(id) * norme2;
        }
        return var/sommePoids;
    }
    

    public static void main(String args[]) throws Exception {
        Scanner in = new Scanner(System.in);
        int playerId = in.nextInt(); // your id (0 to 4)
        in.nextLine();
        
        // le joueur 5 est l'essence, les joueurs 0 � 4
        Player[] joueurs = new Player[6];
        for (int j=0; j<6; j++){
            joueurs[j] = new Player(j);
        }
        
        // les jetons actifs (certains se sont fait mang�s)
        Map<Integer,Jeton> jetonsActifs = new HashMap();
        // les jetons susceptibles de me rencontrer
        Map<Integer,Jeton> jetonsSusceptibles = new HashMap();
        Map<Integer,Double> proximites = new HashMap();
        String jetonsSusceptiblesString;

        // game loop
        while (true) {
            int playerChipCount = in.nextInt(); // The number of chips under your control
            in.nextLine();
            int entityCount = in.nextInt(); // The total number of entities on the table, including your chips
            in.nextLine();
            
            // raz de la liste des jetons actifs
            jetonsActifs.clear();
            
            // parcours des jetons actifs
            for (int i = 0; i < entityCount; i++) {
                int id = in.nextInt(); // Unique identifier for this entity
                int player = in.nextInt(); // The owner of this entity (-1 for neutral droplets)
                double radius = in.nextFloat(); // the radius of this entity
                double x = in.nextFloat(); // the X coordinate (0 to 799)
                double y = in.nextFloat(); // the Y coordinate (0 to 514)
                double vx = in.nextFloat(); // the speed of this entity along the X axis
                double vy = in.nextFloat(); // the speed of this entity along the Y axis
                in.nextLine();
                
                // ajout du jeton � la liste des jetons actifs
                jetonsActifs.put(id, new Jeton (id, player, radius, x, y, vx, vy));
                
                // maj de l'�tat du jeton en cours
                majPlayer(joueurs, id, player, radius, x, y, vx, vy);
            }
            
            // affichage de l'�tat des joueurs
            for (int j=0; j<6; j++){
                joueurs[j].printState(jetonsActifs.keySet());
            }
            
            // pour chacun de nos jetons estimer les jetons susceptibles d'entrer dans notre zone
            Player moi = joueurs[playerId];
            Set<Integer> mesJetonsActifs = moi.jetons.keySet();
            mesJetonsActifs.retainAll(jetonsActifs.keySet());
            for (Integer j : mesJetonsActifs){
                Jeton monJeton = moi.jetons.get(j);
                // regarder les jetons susceptibles de me rencontrer
                jetonsSusceptibles.clear();
                proximites.clear();
                jetonsSusceptiblesString = "( ";
                for (Map.Entry<Integer, Jeton> sonJeton : jetonsActifs.entrySet()) {
                    if (!sonJeton.getKey().equals(j)) {
                        double t = monJeton.rencontre(sonJeton.getValue(),1.0);
                        //double t = 0.5;
                        if (t!=-1) {
                            jetonsSusceptiblesString += sonJeton.getKey() + " : " + String.format( "%.2f",t) + "; ";
                            jetonsSusceptibles.put(sonJeton.getKey(),sonJeton.getValue());
                            proximites.put(sonJeton.getKey(),Double.valueOf(t));
                        }
                    }
                }
                jetonsSusceptiblesString += ")";
                /*System.err.println("Jetons susceptibles de rencontrer mon jeton " + j + " :");
                System.err.println(jetonsSusceptiblesString);*/
                // reflexion avec les jetonsSusceptibles
                estimerMouvementCourt(monJeton,jetonsSusceptibles,proximites);
                /**
                 * Si on n'a pas pris de d�cision pour se d�placer li�e uniquement � la prochaine position
                 * on va devoir estimer la meilleure direction � prendre et si elle vaut le coup d'�tre prise
                 */
                if (!monJeton.seDeplacer){
                    estimerMouvementLong(monJeton,jetonsActifs);
                }
                // donner l'ordre
                if (monJeton.seDeplacer) {
                    System.out.println(monJeton.posVisee.get(0,0) + " " + monJeton.posVisee.get(1,0));
                } else {
                    System.out.println("WAIT");
                }
            }
            
            /*
            // prise de d�cision pour chacun de mes jetons
            for (int i = 0; i < playerChipCount; i++) {
                // Write an action using System.out.println()
                // To debug: System.err.println("Debug messages...");
                System.out.println("WAIT"); // One instruction per chip: 2 real numbers (x y) for a propulsion, or 'WAIT'.
            }*/
        }
    }
    
    public static class Jeton implements Cloneable {
        public int id;
        public int player;
        public double radius;
        public Matrix position;
        public Matrix vitesse;
        public boolean seDeplacer;
        public Matrix posVisee;
        
        public Jeton (int id, int player, double radius, double x, double y, double vx, double vy) {
            this.id = id;
            this.player = player;
            this.radius = radius;
            double[][] pos = {{x},{y}};
            double[][] vit = {{vx},{vy}};
            double[][] posVisee = {{0},{0}};
            this.position = new Matrix(pos);
            this.vitesse = new Matrix(vit);
            this.seDeplacer = false;
            this.vitesse = new Matrix(posVisee);
        }
        
        @Override
        protected Object clone() throws CloneNotSupportedException {
            Jeton cloned = (Jeton)super.clone();
            cloned.position = (Matrix)cloned.position.clone();
            cloned.vitesse = (Matrix)cloned.vitesse.clone();
            cloned.posVisee = (Matrix)cloned.posVisee.clone();
            return cloned;
        }
        
        public void majJeton(double radius, double x, double y, double vx, double vy){
            this.radius = radius;
            double[][] pos = {{x},{y}};
            double[][] vit = {{vx},{vy}};
            this.position = new Matrix(pos);
            this.vitesse = new Matrix(vit);
        }
        
        /* fonction pour savoir s'il est possible de rencontrer un autre jeton au prochain tour (-1 si pas rencontr�)
         * ne prends pas encore en compte les murs
         */
        public double rencontre(Jeton j, double scale) throws Exception {
            
            // le temps de rencontre
            double t=-1;
            
            // revient � un probl�me de la forme at^2 + bt + c <= 0
            double x1 = this.position.get(0,0);
            double y1 = this.position.get(1,0);
            double x2 = x1 + scale * this.vitesse.get(0,0);
            double y2 = y1 + scale * this.vitesse.get(1,0);
            double r1 = this.radius;
            double r2;
            if(this.player == 5){ r2=r1;} else {r2 = r1 + scale*14.2857;}
            
            double x1prime = j.position.get(0,0);
            double y1prime = j.position.get(1,0);
            double x2prime = x1prime + scale * j.vitesse.get(0,0);
            double y2prime = y1prime + scale * j.vitesse.get(1,0);
            double r1prime = j.radius;
            // On regarde si le jeton est passif ou si c'est un joueur
            double r2prime;
            if (j.player == 5) { r2prime = r1prime;} else { r2prime = r1prime + scale*14.2857;}
            
            // premi�re v�rification simple de boite englobante
            double v = Math.max(this.vitesse.norme(), j.vitesse.norme());
            if (2*scale*(v+14.2857)+r1+r1prime > (this.position.moins(j.position)).norme()){
                
                // diff�rents termes du polyn�me at^2 + bt + c
                double a = (x2-x1-x2prime+x1prime)*(x2-x1-x2prime+x1prime)
                        + (y2-y1-y2prime+y1prime)*(y2-y1-y2prime+y1prime)
                        - (r2-r1+r2prime-r1prime)*(r2-r1+r2prime-r1prime);
                double b = 2*(
                        (x1-x1prime)*(x2-x1-x2prime+x1prime)
                        + (y1-y1prime)*(y2-y1-y2prime+y1prime)
                        - (r1+r1prime)*(r2-r1+r2prime-r1prime)
                        );
                double c = (x1-x1prime)*(x1-x1prime) + (y1-y1prime)*(y1-y1prime) - (r1+r1prime)*(r1+r1prime);
                
                // cas d�g�n�r� avec a=0 et b=0
                if (a==0 && b==0){
                    if (c>0) {t=-1;} else {t=0; System.err.println("Probleme cas d�g�n�r� a=0 et b=0");}
                // cas d�g�n�r� avec a=0
                } else if (a==0) {
                    t = -c/b;
                    if (b>0) {
                        if (t>=0) {t=0; System.err.println("Probleme cas d�g�n�r� a=0");} else {t=-1;}
                    } else {
                        if (t<=0) {t=0; System.err.println("Probleme cas d�g�n�r� a=0");} else if (t>1) {t=-1;}
                    }
                // cas non d�g�n�r� (a!=0)
                } else {
                    // v�rification du discriminant
                    double d = b*b - 4*a*c;
                    //System.err.println(j.id + " : d = " + d);
                    // si on a des racines
                    if (d>=0) {
                        // premi�re racine
                        double t1 = (-b-Math.sqrt(d))/(2*a);
                        // deuxieme racine
                        double t2 = (-b+Math.sqrt(d))/(2*a);
                        // rangement dans l'ordre t1<t2
                        double temp1 = t1;
                        t1 = Math.min(t1,t2);
                        t2 = Math.max(temp1,t2);
                        /*System.err.println("   t1 = " + t1);
                        System.err.println("   t2 = " + t2);
                        System.err.println("    a = " + a);*/
                        // cas avec a > 0
                        if (a>=0) {
                            if (t2<0 || t1>1) {
                                t=-1;
                            } else if (t1>0) {
                                t=t1;
                            } else {
                                t=0;System.err.println("Probleme cas d>=0 et a>=0");
                            }
                        // cas avec a < 0
                        } else {
                            if (t1<0 && t2>1) {
                                t=-1;
                            } else if (t1<0 && t2<1) {
                                t=t2;
                            } else {
                                t=0;System.err.println("Probleme cas d>=0 et a<0");
                            }
                        }
                    // si on a pas de racine (discriminant<0)
                    } else {
                        // apparamment un probl�me ici
                        //if (a>0) {t=-1;} else {t=0;System.err.println("Probleme cas d<0");}
                        t=-1;
                    }
                }
            } else {
                //System.err.println(j.id + " : " + (2*(v+14.2857)+r1+r1prime) + " < " + (this.position.moins(j.position)).norme());
            }
            return t;
        }
    }
    
    public static class Matrix implements Cloneable {
        public double[][] matrice; // la matrice
        public int nb_lignes; // le nombre de lignes de la matrice
        public int nb_colonnes; // le nombre de colonnes de la matrice
        
        // construit une matrice avec un tableau
        public Matrix(double[][] tableau) {
            this.matrice = tableau.clone();
            this.nb_lignes = this.matrice.length;
            this.nb_colonnes = this.matrice[0].length;
        }
        
        // construit une matrice de zeros en donnant sa taille
        public Matrix(int nb_lignes, int nb_colonnes) {
            double[][] mat = new double[nb_lignes][nb_colonnes];
            for (int i=0; i<nb_lignes; i++){
                for (int j=0; j<nb_colonnes; j++){
                    mat[i][j] = 0;
                }
            }
            this.matrice = mat;
            this.nb_lignes = this.matrice.length;
            this.nb_colonnes = this.matrice[0].length;
        }
        
        // affichage d'une matrice
        @Override
        public String toString(){
            String s = "{";
            for (int i=0; i<this.nb_lignes; i++){
                s += " {";
                for (int j=0; j<this.nb_colonnes; j++){
                    s += this.get(i,j) + ", ";
                }
                s += "}";
            }
            s += " }";
            return s;
        }
        
        // copie d'un tableau � 2D (la taille de la premi�re D doit avoir �t� pr�cis�e pour dest)
        public static void clone2dArray(double[][] src, double[][] dest) {
            for (int i = 0; i < src.length; i++) {
                System.arraycopy(src[i], 0, dest[i], 0, src[0].length);
            }
        }
        
        @Override
        protected Object clone() throws CloneNotSupportedException{
            Matrix cloned = (Matrix)super.clone();
            double[][] mat = new double[this.nb_lignes][this.nb_colonnes];
            clone2dArray(this.matrice,mat);
            cloned.matrice = mat;
            // v�rifier que le clone ne contient pas les m�mes pointeurs
            /*cloned.matrice[0][0] += 1;
            if(cloned.get(0,0) == this.get(0,0)){throw new CloneNotSupportedException("Probl�me de clonage");}
            cloned.matrice[0][0] -= 1;*/
            return cloned;
        }
        
        public double get(int i, int j) {
            return this.matrice[i][j];
        }
        
        public Matrix plus(Matrix m) throws Exception {
            Matrix m_diff;
            if ((this.nb_lignes-m.nb_lignes!=0) || (this.nb_colonnes-m.nb_colonnes!=0)){
                throw new Exception("Wrong size");
            } else {
                m_diff = (Matrix)this.clone();
                for (int i=0; i<nb_lignes; i++){
                    for (int j=0; j<nb_colonnes; j++){
                        m_diff.matrice[i][j] += m.get(i,j);
                    }
                }
            }
            // v�rifiction que c'est bien une copie
            //if(this.get(0,0) == m_diff.get(0,0)){throw new Exception("Probl�me de clonage");}
            return m_diff;
        }
        
        public Matrix moins(Matrix m) throws Exception {
            Matrix m_diff;
            if ((this.nb_lignes-m.nb_lignes!=0) || (this.nb_colonnes-m.nb_colonnes!=0)){
                throw new Exception("Wrong size");
            } else {
                m_diff = (Matrix)this.clone();
                for (int i=0; i<nb_lignes; i++){
                    for (int j=0; j<nb_colonnes; j++){
                        m_diff.matrice[i][j] -= m.get(i,j);
                    }
                }
            }
            // v�rifiction que c'est bien une copie
            //if(this.get(0,0) == m_diff.get(0,0)){throw new Exception("Probl�me de clonage");}
            return m_diff;
        }
        
        // norme carr� de la matrice
        public double normeCarre() {
            double norme2=0;
            for (int i=0; i<this.nb_lignes; i++){
                for (int j=0; j<this.nb_colonnes; j++){
                    norme2 += (this.matrice[i][j])*(this.matrice[i][j]);
                }
            }
            return norme2;
        }
        
        // norme de la matrice
        public double norme() {
            return Math.sqrt(this.normeCarre());
        }
        
        // produit avec un scalaire
        public Matrix times(Double d) throws Exception {
            Matrix m = (Matrix)this.clone();
            for (int i=0; i < m.nb_lignes; i++) {
                for (int j=0; j < m.nb_colonnes; j++) {
                    m.matrice[i][j] *= d;
                }
            }
            return m;
        }
        
        // produit matricel
        public Matrix times(Matrix m) throws Exception {
            throw new Exception("Wrong size");
        }
    }
}